package store;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class TheStore 
{
	public static Connection cn = null;
	static int id;
	static String option = null;
	 /**
	  * Method to delete data from database
	  * @throws SQLException 
	  */
	// @SuppressWarnings("null")
	public static void DeleteDB() throws SQLException
		{
		
			option = JOptionPane.showInputDialog(null,"Do you want to delete product? Yes or No");
			
			if (option.equalsIgnoreCase("YES"))
			{
				try 
					{
						/**
						 * Call Connection method connectDB() to connect to database
						 */
							
						Connect();
							
							
						//Set auto commit as false.
						cn.setAutoCommit(false);
						// Enter record id to be deleted
						id = Integer.parseInt(JOptionPane.showInputDialog(null,"Enter product id for deletion of a record"));
							
						System.out.println("Creating statement...");
																					
						// delete the data
						String sta1 = "DELETE FROM product  WHERE p_ID = '" + id + "'";
						PreparedStatement sta = cn.prepareStatement(sta1);
						sta.executeUpdate();
							
						JOptionPane.showMessageDialog(null,"Deleting row....");
							
						// if there is no error			
						 cn.commit();
					}
					catch(Exception e)
					{
						// If there is an error then rollback the changes.
						System.err.println("Rolling back data here....");
						cn.rollback();
					}
					// if record is deleted
				System.out.println("Product with Id number: " + id + " deleted.");
			}
			else{
				option = JOptionPane.showInputDialog(null,"Do you want to view product by id? Yes or No");
				
				if (option.equalsIgnoreCase("YES"))
					{
						/*
						 * Call Method SelectVendorProduct to view product
						 */
						SelectVendorProduct();
					}
				else // exit system
				{
					System.out.println("System terminated!!");
					System.exit(0);
				}
			}
						
		}
	 
	 /*
	  * Method to retrive data from database
	  */
	public static void SelectVendorProduct()
	{
		int pHand,pOrder,pUnit;
		String desc;
		try
		{
			// Call Connection method connectDB() to connect to database
			Connect();
			// Prompt user input
			id = Integer.parseInt(JOptionPane.showInputDialog("Enter product id here: "));
			PreparedStatement sta = cn.prepareStatement("SELECT * FROM product where vendor_v_ID = '" + id + "'");
			ResultSet res = sta.executeQuery();
			//JOptionPane.showMessageDialog(null, "database table accessed");
			
			System.out.println("+++=========Product Record(s)========+++");
			
			while (res.next())
			{
				id = res.getInt("p_ID");
				desc = res.getString("desc");
				pHand= res.getInt("p_onhand");
				pOrder = res.getInt("p_reorder_level");
				pUnit = res.getInt("p_unit_price");
				
				
				System.out.println("Product Id: " + id + "| Description: " + desc + "| Product onHand: " + pHand+
						"| Order Levels: " + pOrder+ "| Unit Price: " + pUnit+
						"\n_______________________________________"); 
				
			}
			
			cn.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//JOptionPane.showMessageDialog(null, e.getMessage().toString());
			JOptionPane.showMessageDialog(null, "Not connected to database");
		}
		
	}
	/*
	 * retrieve data from Vendor table 
	 */
	public static void SelectVendor()
	{
		int id, von;
		String name,contact,province;
		try
		{
			// Call Connection method connectDB() to connect to database
			Connect();
			
			PreparedStatement sta = cn.prepareStatement("SELECT * FROM vendor");
			ResultSet res = sta.executeQuery();
			//JOptionPane.showMessageDialog(null, "database table accessed");
			
			System.out.println("+++=========Vendor Records========+++");
			
			while (res.next())
			{
				id = res.getInt("v_ID");
				name = res.getString("v_name");
				contact = res.getString("v_c_no");
				province = res.getString("v_province");
				von = res.getInt("v_orderNo");
				
				
				System.out.println("Vendor Id: " + id + "| Name: " + name + "| Contact No: " + contact+
						"| Province: " + province+ "| order No: " + von+
						"\n_______________________________________"); 
				
			}
			
			cn.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//JOptionPane.showMessageDialog(null, e.getMessage().toString());
			JOptionPane.showMessageDialog(null, "Not connected to database");
		}
		
	}
	
	public static void Connect()
	{
		//Connection conn = null;
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

			cn = (Connection)DriverManager.getConnection("jdbc:mysql://localhost:3306/thmstore","root","thabang");
			
			if(cn != null)
			{
				System.out.println("");
				System.out.println("!!!!!!!!!!!!!!!");
				System.out.println("");
			}
		}
		catch(Exception e)
		{
			System.out.println("not connected to database");
		}
	}
}
