package store;

import java.sql.SQLException;

// import javax.swing.JOptionPane; 
 /** 
  * Main Class 
  * @author Thabang Moepi 
  * 
  */ 
 public class FrontDesk { 

 
 	public static void main(String[] args) throws SQLException  
 	{ 
 		 
		 
 		System.out.println("The following are records of the database"); 
 		/* 
		 * Call methods from class TheStore 
		 */ 
 		 
 		TheStore.SelectVendor(); 
 		TheStore.SelectVendorProduct(); 
 		TheStore.DeleteDB();
 	} 
 
 
 } 
